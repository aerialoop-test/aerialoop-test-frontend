## Next Steps:
- Create Dockerfile to create a development environment without install any libraries.
- Create CI based on Vite to obtain build folder
- Create Terraform IaC file to manage the infraestructure
- Create CD configuration to deploy to S3 - CloudFront service

I think CloudFront is one of the best services to run any frontend because it handles all the networking and clustering features by itself.