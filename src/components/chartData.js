export const dataFetched = async () => {
  let chartDataFetched = await fetch(`http://localhost:3000/flight/chart/${window.location.href.split("/")[4]}`, {
    method: 'get',
    headers: {
      'content-type': 'application/json'
    }
  })
    .then(res => {
      if (!res.ok) {
        const error = new Error(res.statusText);
        throw error;
      }
      let data = res.json();
      return data;
    })
    .catch(err => console.error(err));
  return {
    labels: chartDataFetched.StartTimeHour,
    datasets: [
      {
        label: 'Power Consumption',
        backgroundColor: '#f87979',
        data: chartDataFetched.powerConsumption
      }
    ]
  }
}

export const dataFetchedBarChart = async () => {
  let chartDataFetched = await fetch(`http://localhost:3000/barchart/`, {
    method: 'get',
    headers: {
      'content-type': 'application/json'
    }
  })
    .then(res => {
      if (!res.ok) {
        const error = new Error(res.statusText);
        throw error;
      }
      let data = res.json();
      console.log(data)
      return data;
    })
    .catch(err => console.error(err));
  return {
    labels: chartDataFetched.labels,
    datasets: [
      {
        label: 'Mean Consumption',
        backgroundColor: '#f87979',
        data: chartDataFetched.powerConsumption
      }
    ]
  }
}

export const options = {
  responsive: false,
  maintainAspectRatio: false
}
