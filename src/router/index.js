import { createRouter, createWebHistory } from 'vue-router'
import DataTable from '../views/DataTable.vue'
import FlightData from '../views/FlightData.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: DataTable
    },
    {
      path: '/flight/:id',
      name: 'flight',
      component: FlightData
    }
  ]
})

export default router
